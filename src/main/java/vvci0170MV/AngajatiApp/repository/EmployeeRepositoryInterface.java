package vvci0170MV.AngajatiApp.repository;

import java.util.List;
import vvci0170MV.AngajatiApp.controller.DidacticFunction;
import vvci0170MV.AngajatiApp.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee);
	void modifyEmployeeFunction(Employee employee, DidacticFunction newFunction);
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria();
	Employee findEmployeeById(int idOldEmployee);

}
