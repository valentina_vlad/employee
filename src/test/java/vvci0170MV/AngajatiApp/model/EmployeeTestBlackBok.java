package vvci0170MV.AngajatiApp.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import vvci0170MV.AngajatiApp.controller.DidacticFunction;
import vvci0170MV.AngajatiApp.repository.EmployeeImpl;

import static org.junit.Assert.*;

public class EmployeeTestBlackBok {
    private EmployeeImpl e1;

    @Before
    public void setUp(){
        e1 = new EmployeeImpl();
    }

    @After
    public void tearDown(){
        e1 = null;
    }

    @Test
    public void Test1() { //ECP1 + BVA3
        Employee e = new Employee();
        e.setId(1234);
        e.setFirstName("Morar");
        e.setLastName("Lazar");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setCnp("1244567890876");
        e.setSalary(2560.00);

        assertTrue("This will pass", e1.addEmployee(e));

    }
    @Test
    public void TestCnp() { //ecp3+bva6
        Employee e = new Employee();
        e.setId(1234);
        e.setFirstName("Morar");
        e.setLastName("Lazar");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setCnp("1851209262515656");
        e.setSalary(2560.00);

        assertFalse("This will pass", e1.addEmployee(e));
    }

    @Test
    public void addEmployeeFail() { //testam lungimea numelui
        Employee e = new Employee();
        e.setId(1234);
        e.setFirstName("M");
        e.setLastName("Lazar");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setCnp("1244567890876");
        e.setSalary(2560.00);

        assertFalse("This will fail", e1.addEmployee(e));
    }


    @Test
    public void Test2() { //ECP2+BVA1
        Employee e = new Employee();
        e.setId(1);
        e.setFirstName("Mane");
        e.setLastName("Ion");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setCnp("1244567890876");
        e.setSalary(2000.00);

        assertFalse("This will fail", e1.addEmployee(e));
    }

}