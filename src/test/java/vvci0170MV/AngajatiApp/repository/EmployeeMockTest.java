package vvci0170MV.AngajatiApp.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import vvci0170MV.AngajatiApp.controller.DidacticFunction;
import vvci0170MV.AngajatiApp.model.Employee;
import vvci0170MV.AngajatiApp.repository.EmployeeMock;


import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static vvci0170MV.AngajatiApp.controller.DidacticFunction.LECTURER;
import static vvci0170MV.AngajatiApp.controller.DidacticFunction.TEACHER;

public class EmployeeMockTest {
    private EmployeeMock employeeRepo;

    @Before
    public void setUp(){
        employeeRepo = new EmployeeMock();
    }

    @After
    public void tearDown(){
        employeeRepo = null;
    }

    @Test
    public void TC2() {
        Employee bascja = null;
        List<Employee> empl = employeeRepo.getEmployeeList();
        employeeRepo.modifyEmployeeFunction(bascja, TEACHER);
        assertTrue(empl.equals(employeeRepo.getEmployeeList()));
    }

    @Test
    public void TC1(){

        Employee Ionel = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee Mihai = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee Ionela = new Employee("Gicu", "Ionescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee Mihaela = new Employee("Dodel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee Vasile = new Employee("Dorel", "Georgescu", "1234567890876", DidacticFunction.TEACHER, 2500d);
        Employee Marin   = new Employee("Larson", "Puscas", "1234567890876", DidacticFunction.TEACHER,  2500d);
        employeeRepo.modifyEmployeeFunction(Ionel, DidacticFunction.LECTURER);
        assertEquals(LECTURER, employeeRepo.findEmployeeById(0).getFunction());
    }
}